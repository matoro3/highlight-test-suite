#!/bin/bash

# this test script will be executed by the highlight Gitlab CI script

for f in `dirname $0`/syntax_test_*; do
	highlight $f
	if [ $? -eq 0 ]; then
		echo "$f OK"
	else
		echo "$f FAILED"
		exit 1
	fi
done

:result: dsadsa

Pre- and postfix headings:

= Heading 1 =
== Heading 2 ==
=== Heading 3 ===

Underlined headings:

Heading 1
=========
# <  kwa
fdsfds fds fds fds fds fsd fds gfsdg ds sdfg fsdg sd gsd sd s


Heading 2
~~~~~~~~~
# <  kwa
fdsfds fds fds fds fds fsd fds gfsdg ds sdfg fsdg sd gsd sd s

Heading 3
*********
# <  kwa

#+BEGIN_COMMENT
fdsfds fds fds fds fds fsd fds gfsdg ds sdfg fsdg sd gsd sd s
#+END_COMMENT
# <  com

#+title:  Example Org File
#+author: TEC
#+date:   2020-10-27
# <  kwb

* Revamp orgmode.org website
# <  kwd


The /beauty/ of org *must* be shared.
#   ^^^^^^^^        ^^^^^^  kwe

*bold*
# <  kwe

/italic/
# <  kwe

_underline_
# <  kwe

=code=
# <  kwe

~verbatim~
# <  kwe

+strike-through+
# <  kwe


[[https://upload.wikimedia.org/wikipedia/commons/b/bd/Share_Icon.svg]]

** DONE Make screenshots

   CLOSED: [2020-09-03 Thu 18:24]

** DONE Restyle Site CSS


Go through [[file:style.scss][stylesheet]]

** TODO Check CSS on main pages

* Learn Org


Org makes easy things trivial and complex things practical.

You don't need to learn Org before using Org: read the quickstart
page and you should be good to go.  If you need more, Org will be
here for you as well: dive into the manual and join the community!


** Feedback


#+include: "other/feedback.org*manual" :only-contents t

* Check CSS minification ratios


#+BEGIN_SRC python

from pathlib import Path
# <  kwa
cssRatios = []
for css_min in Path("resources/style").glob("*.min.css"):
#                    ^^^^^^^^^^^^^^^  str
    css = css_min.with_suffix('').with_suffix('.css')
    cssRatios.append([css.name,
    "{:.0f}% minified ({:4.1f} KiB)".format( 100 *
#    ^^^^^^ ipl ^^^^^  str
                      css_min.stat().st_size / css.stat().st_size,
                      css_min.stat().st_size / 1000)])
return cssRatios

#+END_SRC

#+include: "other/feedback.org*manual" :only-contents t

* Check CSS minification ratios

#+begin_src sh :results list
#!/bin/bash
#  Upload file pair (Filename.lsm, Filename.tar.gz)
#+ to incoming directory at Sunsite/UNC (ibiblio.org).

E_ARGERROR=85

if [ -z "$1" ]
then
# ^ kwa
  echo "Usage: `basename $0` Filename-to-upload"
#  ^ kwb
  exit $E_ARGERROR
fi

Filename=`basename $1`           # Strips pathname out of file name.

Server="ibiblio.org"
Directory="/incoming/Linux"

Password="your.e-mail.address"   # Change above to suit.

ftp -n $Server <<End-Of-Session

user anonymous "$Password"       #  If this doesn't work, then try:
                                 #  quote user anonymous "$Password"
binary
bell                             # Ring 'bell' after each file transfer.
cd $Directory
put "$Filename.lsm"
put $Filename.tar.gz
bye
End-Of-Session

exit 0
#+end_src

#+include: "other/feedback.org*manual" :only-contents t

* Check CSS minification ratios


#+begin_src ruby

module A
  def a1
    puts 'a1 is called'
  end
end

module B
  def b1
    puts 'b1 is called'
  end
end

module C
  def c1
    puts 'c1 is called'
  end
end

class Test
  include A
  include B
  include C

  def display
    puts 'Modules are included'
  end
end

object=Test.new
object.display
object.a1
object.b1
object.c1

#+end_src

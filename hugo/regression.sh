#!/bin/bash

# this test script will be executed by the highlight Gitlab CI script

for f in `dirname $0`/syntax_test_*; do

	if [ "$(basename "$f")" = "syntax_test_strings.hug" ] || [ "$(basename "$f")" = "syntax_test_interpolation-utf8.hug" ]; then
		echo "UTF-8"
		highlight -u utf-8 $f
	else
		echo "ASCII"
		highlight $f
	fi

	if [ $? -eq 0 ]; then
		echo "$f OK"
	else
		echo "$f FAILED"
		exit 1
	fi

done
